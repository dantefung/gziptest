package com.jinchange.gzip.entity;

import lombok.Data;

/**
 * @ClassName: Project
 * @Author zhangjin
 * @Date 2022/3/24 20:42
 * @Description:
 */
@Data
public class Advertising {
    private String adName;
    private String adTag;
}
